## COMP30750 Information Visualisation

Rajit Banerjee, 18202817.  
Spring 2021.  

Data sets for visualisation tasks.

-   [food_consumption.csv](./food_consumption.csv): Slight modification applied to the [original dataset](https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2020/2020-02-18/food_consumption.csv) to replace all occurrences of 'USA' with 'United States of America'.
-   [country_averages.csv](./country_averages.csv): Aggregation applied to `food_consumption.csv` to group the rows by country, so that each row represents the average food consumption and CO2 emission for a country, averaged over 11 food categories.
